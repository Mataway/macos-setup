#!env bash

# check to see if ansible is installed; if not do the needful thing
export PY_USER_BIN=$(python -c 'import site; print(site.USER_BASE + "/bin")')
export PATH=$PY_USER_BIN:$PATH
ansible --version > /dev/null 2>&1
if (($?  != 0 ));
then
  echo "Installing pip and ansible"
  sudo easy_install pip
  pip install --user ansible
fi

ansible-playbook -i ./hosts playbook.yml --verbose
