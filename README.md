## macos-setup

This is an [Ansible](https://www.ansible.com/) playbook to quickly setup 
a Mac how I like it. This system was built on top of a similar project by Daemonza:

* https://github.com/daemonza/setupmac

This setup is heavily inspired by the setup I used while a client at Pivotal Labs. They
have their own projects for managing machines hosted here:

* https://github.com/pivotal/workstation-setup - powered by Bash
* https://github.com/pivotal-sprout/sprout-wrap - powered by Chef

To setup a machine, clone this repo and run the following command :
```
bash setup.bash
```

## Awesome resources found while working on this
https://www.ukuug.org/events/linux2003/papers/bash_tips/
https://blog.vandenbrand.org/2016/01/04/how-to-automate-your-mac-os-x-setup-with-ansible/
https://github.com/geerlingguy/mac-dev-playbook


## Todos
* Add ImageOptim: https://imageoptim.com/mac
* Add Cura slicer: https://ultimaker.com/software/ultimaker-cura/
